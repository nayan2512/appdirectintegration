package com.appdirect.oauthsecurity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth.consumer.ProtectedResourceDetails;
import org.springframework.security.oauth.consumer.client.OAuthRestTemplate;
import org.springframework.stereotype.Component;

import com.appdirect.domain.Address;
import com.appdirect.domain.Company;
import com.appdirect.domain.Creator;
import com.appdirect.domain.Item;
import com.appdirect.domain.MarketPlace;
import com.appdirect.domain.Order;
import com.appdirect.domain.Payload;
import com.appdirect.domain.SubscriberNotification;
import com.appdirect.domain.SubscriptionAccount;

@Component
public class Oauth2LeggedClient implements IOauth2LeggedClient {

	@Autowired
	ProtectedResourceDetails protectedResourceDetails;
	
	@Override
	public SubscriberNotification getSubscriberNotification(String url) {
		
		OAuthRestTemplate template = new OAuthRestTemplate(protectedResourceDetails);
		
		//GET THIS NOTIFICATION FROM APPDIRECT END POINT, need to be tested after hosting it in server
		//SubscriberNotification subscriberNotification = template.getForObject(url,SubscriberNotification.class);
		
		//Dummy notification
		SubscriberNotification subscriberNotification = getDummySubscriptionNotification(url);
        return subscriberNotification;
	}
	
	private SubscriberNotification getDummySubscriptionNotification(String url) {
		
		MarketPlace marketPlace = new MarketPlace("https://www.acme.com","APPDIRECT");
		Address address= new  Address("San Jose", "USA", "Test",
				"Test User", "User", "CA","1 Main St","95313");
		Creator creator = new Creator(address, "testuser@testco.com", "Test",
				"User", "en", "en_US", "https://www.acme.com/openid/id/47cb8f55-1af6-5bfc-9a7d-8061d3aa0c97",
				"47cb8f55-1af6-5bfc-9a7d-8061d3aa0c97");
		Company company = new Company("17654","US", "tester", "385beb51-51ae-4ffe-8c05-3f35a9f99825", "www.testco.com","1-800-333-3333");
		
		Item item = new Item("4","USER");
		Order order = new Order(item,"Standard", "MONTHLY");
		SubscriptionAccount account = new SubscriptionAccount("","ACTIVE",null);
		Payload payload =  new Payload(company,order,account);
		
		SubscriberNotification subscriberNotification =  new SubscriberNotification("SUBSCRIPTION_ORDER", marketPlace,creator,payload);
		return subscriberNotification;
     }
}
