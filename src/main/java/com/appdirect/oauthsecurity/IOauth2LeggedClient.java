package com.appdirect.oauthsecurity;

import com.appdirect.domain.SubscriberNotification;

public interface IOauth2LeggedClient {

	public SubscriberNotification getSubscriberNotification(String url);
}
