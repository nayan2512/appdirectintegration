package com.appdirect.oauthsecurity;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth.common.signature.SharedConsumerSecretImpl;
import org.springframework.security.oauth.consumer.BaseProtectedResourceDetails;
import org.springframework.security.oauth.consumer.ProtectedResourceDetails;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class OauthWebSecurityConfiguration  extends WebSecurityConfigurerAdapter {

	    private String oauthKey = "testabc-140834";

	    private String oauthSecret = "I2jSpBHjawCQ";

	    @Override
	    protected void configure(HttpSecurity http) throws Exception {
	    	http.csrf().disable();
	        http.authorizeRequests().antMatchers("/**").permitAll().anyRequest().authenticated();
	    }

	    @Bean
	    public ProtectedResourceDetails protectedResourceDetails() {
	        BaseProtectedResourceDetails resourceDetails = new BaseProtectedResourceDetails();
	        resourceDetails.setConsumerKey(oauthKey);
	        resourceDetails.setSharedSecret(new SharedConsumerSecretImpl(oauthSecret));
	        return resourceDetails;
	    }
}
