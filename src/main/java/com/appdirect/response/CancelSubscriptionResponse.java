package com.appdirect.response;

public class CancelSubscriptionResponse extends Response {
	
	String message;

	public CancelSubscriptionResponse(String message, String success) {
		super();
		this.message = message;
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
