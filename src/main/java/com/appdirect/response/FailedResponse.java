package com.appdirect.response;

public class FailedResponse extends Response {

	String errorCode;
	String message;
	
	public String getErrorCode() {
		return errorCode;
	}


	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public FailedResponse(String errorCode, String message,String success) {
		super();
		this.errorCode = errorCode;
		this.message = message;
		this.success = success;
	}
	
	
}
