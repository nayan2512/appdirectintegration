package com.appdirect.response;

public class CreateSubscriptionResponse extends Response {

	String accountIdentifier;
  	
	public CreateSubscriptionResponse(String accountIdentifier, String success) {
		super();
		this.accountIdentifier = accountIdentifier;
		this.success = success;
	}
	
	public String getAccountIdentifier() {
		return accountIdentifier;
	}
	public void setAccountIdentifier(String accountIdentifier) {
		this.accountIdentifier = accountIdentifier;
	}
	public String getSuccess() {
		return success;
	}
	public void setSuccess(String success) {
		this.success = success;
	}
 	
}
