package com.appdirect.response;

public class AssignUserResponse extends Response {

	String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public AssignUserResponse(String message,String sucess) {
		super();
		this.message = message;
		this.success = sucess;
	}
}
