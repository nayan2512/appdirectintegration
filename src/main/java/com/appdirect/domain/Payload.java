package com.appdirect.domain;

public class Payload {

	Company company;
	Order order;
	SubscriptionAccount account;
    String notice;
	
	
	public Payload(Company company, Order order, SubscriptionAccount subscriptionAccount) {
		super();
		this.company = company;
		this.order = order;
	}
	
	public SubscriptionAccount getAccount() {
		return account;
	}

	public void setAccount(SubscriptionAccount account) {
		this.account = account;
	}

	public String getNotice() {
		return notice;
	}

	public void setNotice(String notice) {
		this.notice = notice;
	}

	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
}
