package com.appdirect.domain;

public class SubscriberNotification {

	String type;
	MarketPlace marketplace;
	Creator creator;
	Payload payload;
	
	
	public SubscriberNotification(String type, MarketPlace marketplace,
			Creator creator, Payload payload) {
		super();
		this.type = type;
		this.marketplace = marketplace;
		this.creator = creator;
		this.payload = payload;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public MarketPlace getMarketplace() {
		return marketplace;
	}
	public void setMarketplace(MarketPlace marketplace) {
		this.marketplace = marketplace;
	}
	public Creator getCreator() {
		return creator;
	}
	public void setCreator(Creator creator) {
		this.creator = creator;
	}
	public Payload getPayload() {
		return payload;
	}
	public void setPayload(Payload payload) {
		this.payload = payload;
	}
}
