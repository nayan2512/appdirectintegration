package com.appdirect.domain;

public class Order {

	String editionCode;
	String pricingDuration;
	Item item;
	
	
	public Order(Item item, String editionCode, String pricingDuration) {
		super();
		this.editionCode = editionCode;
		this.pricingDuration = pricingDuration;
		this.item = item;
	}
	
	public String getEditionCode() {
		return editionCode;
	}
	public void setEditionCode(String editionCode) {
		this.editionCode = editionCode;
	}
	public String getPricingDuration() {
		return pricingDuration;
	}
	public void setPricingDuration(String pricingDuration) {
		this.pricingDuration = pricingDuration;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}
}
