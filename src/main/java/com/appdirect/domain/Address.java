package com.appdirect.domain;

public class Address {

	String city;
    String country;
    String firstName;
    String fullName;
    String lastName;
    String state;
    String street1;
    String zip;
        	
	public Address(String city, String country, String firstName,
			String fullName, String lastName, String state, String street1,
			String zip) {
		super();
		this.city = city;
		this.country = country;
		this.firstName = firstName;
		this.fullName = fullName;
		this.lastName = lastName;
		this.state = state;
		this.street1 = street1;
		this.zip = zip;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStreet1() {
		return street1;
	}
	public void setStreet1(String street1) {
		this.street1 = street1;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
}
