package com.appdirect.domain;

public class Company {

	String id;
	String country;
    String name;
    String uuid;
    String website;
    String phoneNumber;
     
    
	public Company(String id,String country, String name, String uuid, String website,String phoneNumber) {
		super();
		this.country = country;
		this.name = name;
		this.uuid = uuid;
		this.website = website;
		this.id = id;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
}
