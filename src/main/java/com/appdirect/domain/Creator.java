package com.appdirect.domain;

public class Creator {

	Address address;
	String email;;
	String firstName;
	String lastname;
	String language;
	String locale;
	String openID;
	String uuid;
	
	
	public Creator(Address address, String email, String firstName,
			String lastname, String language, String locale, String openID,
			String uuid) {
		super();
		this.address = address;
		this.email = email;
		this.firstName = firstName;
		this.lastname = lastname;
		this.language = language;
		this.locale = locale;
		this.openID = openID;
		this.uuid = uuid;
	}
	
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	public String getOpenID() {
		return openID;
	}
	public void setOpenID(String openID) {
		this.openID = openID;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
}
