package com.appdirect.domain;

public class MarketPlace {

	String baseUrl;
	String partner;

	
	public MarketPlace(String baseUrl, String partner) {
		super();
		this.baseUrl = baseUrl;
		this.partner = partner;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public String getPartner() {
		return partner;
	}

	public void setPartner(String partner) {
		this.partner = partner;
	}
}
