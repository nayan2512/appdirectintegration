package com.appdirect.domain;

public class Account {

	
	String email;;
	String firstName;
	String lastname;
	String language;
	String locale;
	String uuid;
	String openId;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	
	
	public Account(String email, String firstName, String lastname,
			String language, String locale, String openId, String uuid) {
		super();
		this.email = email;
		this.firstName = firstName;
		this.lastname = lastname;
		this.language = language;
		this.locale = locale;
		this.uuid = uuid;
		this.openId = openId;
		
	}
	
		
}
