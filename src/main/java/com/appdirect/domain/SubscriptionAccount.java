package com.appdirect.domain;

import java.util.List;

public class SubscriptionAccount {

	private String accountIdentifier;
    private String status;
    private List<Account> users;
    
    
	public List<Account> getUsers() {
		return users;
	}
	public void setUsers(List<Account> users) {
		this.users = users;
	}
	public String getAccountIdentifier() {
		return accountIdentifier;
	}
	public void setAccountIdentifier(String accountIdentifier) {
		this.accountIdentifier = accountIdentifier;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public SubscriptionAccount(String accountIdentifier, String status,
			List<Account> users) {
		super();
		this.accountIdentifier = accountIdentifier;
		this.status = status;
		this.users = users;
	}
	
	public SubscriptionAccount() {
		
	}
}
