package com.appdirect.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.appdirect.domain.SubscriberNotification;
import com.appdirect.enums.error.SubscriptionErrorCodes;
import com.appdirect.oauthsecurity.Oauth2LeggedClient;
import com.appdirect.response.CancelSubscriptionResponse;
import com.appdirect.response.CreateSubscriptionResponse;
import com.appdirect.response.FailedResponse;
import com.appdirect.response.Response;
import com.appdirect.service.ISubscriptionService;
import com.wordnik.swagger.annotations.Api;

@RestController
@Api(value = "subscriptions", description = "Subscription API")
public class SubscriptionController extends AbstractRestHandler {

	
	@Autowired
	ISubscriptionService subscriptionService;
	
	@Autowired
	Oauth2LeggedClient oAuth2LeggedClient; 
	
	    @RequestMapping(value = "/api/v1/subscriptions",
	            method = RequestMethod.POST,
	            consumes = {"application/json"},
	            produces = {"application/json"})
	    @ResponseBody
	    public ResponseEntity<Response> createSubscription(@RequestParam("eventUrl") String eventUrl) throws Exception {
		 
		 final SubscriberNotification subscriberNotification = oAuth2LeggedClient.getSubscriberNotification(eventUrl);
		 try {
			  if(!subscriptionService.checkIfSubscriptionExists(subscriberNotification.getCreator().getOpenID())) {
				 String accountIdentifier = subscriptionService.createSubscription(subscriberNotification);
				 return new ResponseEntity<Response>(new CreateSubscriptionResponse(accountIdentifier, "true"),HttpStatus.OK);
			  }
			  else {
				 return new ResponseEntity<Response>( new FailedResponse(SubscriptionErrorCodes.USER_ALREADY_EXISTS.toString(), SubscriptionErrorCodes.USER_ALREADY_EXISTS.getErrorMessage(), "false"),HttpStatus.OK);
			  }
			  
		  } catch (Exception e) {
			     return new ResponseEntity<Response>( new FailedResponse(SubscriptionErrorCodes.UNKOWN_ERROR_OCCURED.toString(), e.getMessage(), "false"),HttpStatus.OK);
		  }
	    }
	 
	    @RequestMapping(value = "/api/v1/subscriptions",
	            method = RequestMethod.DELETE,
	            consumes = {"application/json"},
	            produces = {"application/json"})
	    @ResponseBody
	    public ResponseEntity<Response> cancelSubscription(@RequestParam("eventUrl") String eventUrl) throws Exception {

		 final SubscriberNotification subscriberNotification = oAuth2LeggedClient.getSubscriberNotification(eventUrl);
		 
		 try {
			   if(subscriptionService.checkIfSubscriptionExists(subscriberNotification.getCreator().getOpenID())) {
			      subscriptionService.cancelSubscription(subscriberNotification);
			      return new ResponseEntity<Response>(new CancelSubscriptionResponse("Your subscription has been cancelled sucessfully","true"),HttpStatus.OK);
			   } else {
				  return new ResponseEntity<Response>( new FailedResponse(SubscriptionErrorCodes.ACCOUNT_NOT_FOUND.toString(), SubscriptionErrorCodes.ACCOUNT_NOT_FOUND.getErrorMessage(), "false"),HttpStatus.OK);
			   }
			   
		  } catch (Exception e) {
		     return new ResponseEntity<Response>( new FailedResponse(SubscriptionErrorCodes.UNKOWN_ERROR_OCCURED.toString(), e.getMessage(), "false"),HttpStatus.OK);
	       }
	     }
	    
	    
	    @RequestMapping(value = "/api/v1/subscriptions",
	            method = RequestMethod.PUT,
	            consumes = {"application/json"},
	            produces = {"application/json"})
	    @ResponseBody
	    public ResponseEntity<Response> changeSubscription(@RequestParam("eventUrl") String eventUrl) throws Exception {

		 final SubscriberNotification subscriberNotification = oAuth2LeggedClient.getSubscriberNotification(eventUrl);
		 
		 try {
			   if(subscriptionService.checkIfSubscriptionExists(subscriberNotification.getCreator().getOpenID())) {
				   String accountIdentifier = subscriptionService.changeSubscription(subscriberNotification);
			      return new ResponseEntity<Response>(new CreateSubscriptionResponse("Subscription has been changed sucessfully with account identifier " + accountIdentifier,"true"),HttpStatus.OK);
			   } else {
				  return new ResponseEntity<Response>( new FailedResponse(SubscriptionErrorCodes.ACCOUNT_NOT_FOUND.toString(), SubscriptionErrorCodes.ACCOUNT_NOT_FOUND.getErrorMessage(), "false"),HttpStatus.OK);
			   }
			   
		  } catch (Exception e) {
		     return new ResponseEntity<Response>( new FailedResponse(SubscriptionErrorCodes.UNKOWN_ERROR_OCCURED.toString(), e.getMessage(), "false"),HttpStatus.OK);
	       }
	     }
	    
}