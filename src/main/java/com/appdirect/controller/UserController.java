package com.appdirect.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.appdirect.response.AssignUserResponse;
import com.appdirect.response.FailedResponse;
import com.appdirect.response.Response;
import com.appdirect.response.UnAssignUserResponse;
import com.appdirect.service.IUserService;
import com.wordnik.swagger.annotations.Api;

@RestController
@Api(value = "Users", description = "User API")
public class UserController extends AbstractRestHandler {

	@Autowired
	IUserService userService;
	
	@RequestMapping(value = "/api/account/v1/companies/{companyId}/users/{userId}/assign/{subscriptionId}",
            method = RequestMethod.POST,
            consumes = {"application/json"},
            produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Response> assignUserToSubscription(@PathVariable("companyId") String companyId, @PathVariable("userId") String userId, @PathVariable("subscriptionId") String subscriptionId) throws Exception {
		
		try {
		       userService.assignUserToSubscription(userId,companyId,subscriptionId);
		       return new ResponseEntity<Response>(new AssignUserResponse("User Assigned successfully", "true"),HttpStatus.OK);
		} catch(Exception e) {
			   return new ResponseEntity<Response>( new FailedResponse("121",e.getMessage(), "false"),HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/api/account/v1/companies/{companyId}/users/{userId}/unassign/{subscriptionId}",
            method = RequestMethod.POST,
            consumes = {"application/json"},
            produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Response> unAssignUserToSubscription(@PathVariable("companyId") String companyId, @PathVariable("userId") String userId, @PathVariable("subscriptionId") String subscriptionId) throws Exception {
		
		try {
		       userService.unAssignUserToSubscription(userId,companyId,subscriptionId);
		       return new ResponseEntity<Response>(new UnAssignUserResponse("User Unassigned successfully", "true"),HttpStatus.OK);
		} catch(Exception e) {
			   return new ResponseEntity<Response>( new FailedResponse("121",e.getMessage(), "false"),HttpStatus.OK);
		}
	}
	
}
