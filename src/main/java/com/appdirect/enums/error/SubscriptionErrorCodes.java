package com.appdirect.enums.error;

public enum SubscriptionErrorCodes {

	USER_ALREADY_EXISTS(101, "User already exists."),
	USER_NOT_FOUND(102, "User not found."),
	ACCOUNT_NOT_FOUND(103,"Account not found"),
	UNKOWN_ERROR_OCCURED(104,"Unknown Error occured");

    int errorCode;
    String errorMessage;

    SubscriptionErrorCodes(int errorCode, String errorMessage) {
        this.errorMessage = errorMessage;
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public static String getErrorMessageAssociatedWithErrorCode(int value) {
        for(SubscriptionErrorCodes errorCodes : values()) {
            if(errorCodes.getErrorCode() == value) {
                return errorCodes.getErrorMessage();
            }
        }

        throw new IllegalArgumentException("Invalid error code specified: " + value);
    }
}
