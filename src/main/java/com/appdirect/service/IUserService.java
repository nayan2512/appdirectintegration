package com.appdirect.service;

public interface IUserService {

	public void assignUserToSubscription(String userId, String companyId, String subscriptionId) throws Exception;

	public void unAssignUserToSubscription(String userId, String companyId, String subscriptionId) throws Exception;
}
