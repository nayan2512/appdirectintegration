package com.appdirect.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserService implements IUserService {

	@Autowired
	ISubscriptionService subscriptionService;
	
	@Override
	public void assignUserToSubscription(String userId, String companyId, String subscriptionId) throws Exception {
		subscriptionService.assignUserToSubscription(userId, companyId, subscriptionId);
	}

	@Override
	public void unAssignUserToSubscription(String userId, String companyId,String subscriptionId) throws Exception {
		subscriptionService.unAssignUserToSubscription(userId, companyId, subscriptionId);

	}

}
