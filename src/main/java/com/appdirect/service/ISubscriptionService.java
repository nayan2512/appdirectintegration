package com.appdirect.service;

import com.appdirect.domain.SubscriberNotification;

public interface ISubscriptionService {

	
	public String createSubscription(SubscriberNotification subscriberNotification) throws Exception;

	public void cancelSubscription(SubscriberNotification subscriberNotification) throws Exception;
	
	public Boolean checkIfSubscriptionExists(String openId) throws Exception;

	public String changeSubscription(SubscriberNotification subscriberNotification) throws Exception;
	
	public void assignUserToSubscription(String userId, String companyId, String subscriptionId) throws Exception;

	public void unAssignUserToSubscription(String userId, String companyId,String subscriptionId) throws Exception;
}
