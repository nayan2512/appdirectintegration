package com.appdirect.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.appdirect.domain.Account;
import com.appdirect.domain.Company;
import com.appdirect.domain.Creator;
import com.appdirect.domain.SubscriberNotification;
import com.appdirect.domain.SubscriptionAccount;

@Component
public class SubscriptionService implements ISubscriptionService {

	private Map<String,Map<String,SubscriptionAccount>> accountSubscriptionInformation = new HashMap<String, Map<String,SubscriptionAccount>>();
	private Map<String,SubscriptionAccount> accountData = new HashMap<String, SubscriptionAccount>();
	private Map<String, Company> companyData = new HashMap<String, Company>();
	private Map<String, Account> userData = new HashMap<String, Account>();
	private List<Account> userList = new ArrayList<Account>();
	
	@Override
	public String createSubscription(final SubscriberNotification subscriberNotification) throws Exception {
		
		String accountIdentifier = generateAccountIdentifier();
		String openId = subscriberNotification.getCreator().getOpenID();
		
		SubscriptionAccount subscriberAccount = new SubscriptionAccount();
		subscriberAccount.setAccountIdentifier(accountIdentifier);
		subscriberAccount.setStatus(subscriberNotification.getType());
		subscriberAccount.setUsers(getAdminForThisAccountAsUser(subscriberNotification.getCreator()));
		saveSubscription(accountIdentifier,openId,subscriberAccount, subscriberNotification);
		return accountIdentifier;
	}

    @Override
	public void cancelSubscription(final SubscriberNotification subscriberNotification) {
        accountSubscriptionInformation.remove(subscriberNotification.getCreator().getOpenID());
	}


	@Override
	public Boolean checkIfSubscriptionExists(String openId) throws Exception {
		return (accountSubscriptionInformation.get(openId) != null) ? true : false;
	}

	@Override
	public String changeSubscription(final SubscriberNotification subscriberNotification) throws Exception {
		SubscriptionAccount oldSubscriptionAccount = accountSubscriptionInformation.get(subscriberNotification.getCreator().getOpenID()).get(subscriberNotification.getPayload().getAccount().getAccountIdentifier());
		oldSubscriptionAccount.setStatus(subscriberNotification.getType());
		saveSubscription(subscriberNotification.getPayload().getAccount().getAccountIdentifier(), subscriberNotification.getCreator().getOpenID(), oldSubscriptionAccount, subscriberNotification);
		return subscriberNotification.getPayload().getAccount().getAccountIdentifier();
	}
	
	@Override
	public void assignUserToSubscription(String userId, String companyId, String subscriptionId) throws Exception {
		if(checkIfUserExists(userId)) throw new Exception("User does not exists"); //This could have been our own custom exception
		if(checkIfSubscriptionExists(subscriptionId)) throw new Exception("Subscription does not exists"); //This could have been our own custom exception
		
		Account newUserAssign = getUser(userId);
		SubscriptionAccount existingSubscription = getExistingSubscription(subscriptionId);
		//Add the new user to subscription
		
		List<Account> users = existingSubscription.getUsers();
		users.add(newUserAssign);
		existingSubscription.setUsers(users);
	}
	
	@Override
	public void unAssignUserToSubscription(String userId, String companyId, String subscriptionId) throws Exception {
		if(checkIfUserExists(userId)) throw new Exception("User does not exists"); //This could have been our own custom exception
		if(checkIfSubscriptionExists(subscriptionId)) throw new Exception("Subscription does not exists"); //This could have been our own custom exception
		
		Account newUserAssign = getUser(userId);
		SubscriptionAccount existingSubscription = getExistingSubscription(subscriptionId);
		//Remove the new user to subscription
		
		List<Account> users = existingSubscription.getUsers();
		users.remove(newUserAssign);
		existingSubscription.setUsers(users);
	}
	
	private List<Account> getAdminForThisAccountAsUser(Creator creator) {
		Account account =  new Account(creator.getEmail(), creator.getFirstName(), creator.getLastname(), creator.getLanguage(), creator.getLocale(), creator.getOpenID(), creator.getUuid());
		return Arrays.asList(account);
	}

	private String generateAccountIdentifier() {
		return UUID.randomUUID().toString();
	}
	
	private void saveSubscription(String accountIdentifier,String openId,final SubscriptionAccount subscriberAccount, final SubscriberNotification subscriberNotification) {
		accountData.put(subscriberAccount.getAccountIdentifier(), subscriberAccount); 
		companyData.put(subscriberNotification.getPayload().getCompany().getId(), subscriberNotification.getPayload().getCompany());
		userList.addAll(subscriberAccount.getUsers());
		Map<String,SubscriptionAccount>  subscribingAccount = new HashMap<String, SubscriptionAccount>();
		subscribingAccount.put(accountIdentifier, subscriberAccount);
		accountSubscriptionInformation.put(openId,subscribingAccount);
	}

	private Boolean checkIfUserExists(String userId) {
		for(Account account : userList) {
			if(account.getUuid().equalsIgnoreCase(userId))
				 return true;
		}
		return false;
	}
	
	private Account getUser(String userId) {
		for(Account account : userList) {
			if(account.getUuid().equalsIgnoreCase(userId))
				 return account;
		}
		return null;
	}
	
	private SubscriptionAccount getExistingSubscription(String subscriptionId) {
		for(Entry<String, Map<String, SubscriptionAccount>> accountInformation : accountSubscriptionInformation.entrySet()) {
			Map<String,SubscriptionAccount> account = accountInformation.getValue();
			 if(account.containsKey(subscriptionId))
				  return account.get(subscriptionId);
		}
		return null;
	}
}
