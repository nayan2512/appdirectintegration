package com.appdirect.integrationtest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.appdirect.Application;
import com.appdirect.controller.SubscriptionController;
import com.appdirect.oauthsecurity.Oauth2LeggedClient;
import com.appdirect.response.CancelSubscriptionResponse;
import com.appdirect.response.CreateSubscriptionResponse;
import com.appdirect.response.FailedResponse;
import com.appdirect.response.Response;
import com.appdirect.service.SubscriptionService;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest("server.port:8090")
public class SubscriptionIntegrationTest {

	SubscriptionService subscriptionService;
	
	Oauth2LeggedClient oAuth2LeggedClient; 
	
	SubscriptionController subsController;
	
	@Before
	public void init() throws Exception {
		subsController = new SubscriptionController();
		subscriptionService = new SubscriptionService();
		oAuth2LeggedClient = new Oauth2LeggedClient();
	}
	
	@Test
	public void createSubscription_ValidEventUrl_Success() throws Exception {
		ResponseEntity<Response> res = subsController.createSubscription("https://www.acme.com/openid/id/47cb8f55-1af6-5bfc-9a7d-8061d3aa0c97");
		CreateSubscriptionResponse createResponse = (CreateSubscriptionResponse) res.getBody();
		assert (res != null);
		assert (createResponse.getAccountIdentifier() != null);
	}
	
	@Test
	public void createSubscription_ValidEventUrl_Failure() throws Exception {
		//First create a subscription
		ResponseEntity<Response> res = subsController.createSubscription("https://www.acme.com/openid/id/47cb8f55-1af6-5bfc-9a7d-8061d3aa0c97");
		
		//Create second one which would failed
		ResponseEntity<Response> resFailed = subsController.createSubscription("https://www.acme.com/openid/id/47cb8f55-1af6-5bfc-9a7d-8061d3aa0c97");
		FailedResponse failedResponse = (FailedResponse) res.getBody();
		assert (res != null);
		assert (failedResponse.getMessage() == "User already exists.");
	}
	
	@Test
	public void cancelSubscription_ValidEventUrl_Failure() throws Exception {
		ResponseEntity<Response> res = subsController.cancelSubscription("https://www.acme.com/openid/id/47cb8f55-1af6-5bfc-9a7d-8061d3aa0c97");
		FailedResponse failedResponse = (FailedResponse) res.getBody();
		assert (res != null);
		assert (failedResponse.getMessage() == "Account not found" );
	}
	
	@Test
	public void cancelSubscription_ValidEventUrl_Success() throws Exception {
		//First create a subscription
		ResponseEntity<Response> res = subsController.createSubscription("https://www.acme.com/openid/id/47cb8f55-1af6-5bfc-9a7d-8061d3aa0c97");
		
		//Cancel the subscription
		ResponseEntity<Response> resFailed = subsController.cancelSubscription("https://www.acme.com/openid/id/47cb8f55-1af6-5bfc-9a7d-8061d3aa0c97");
		CancelSubscriptionResponse cancelSubsResponse = (CancelSubscriptionResponse) res.getBody();
		assert (res != null);
		assert (cancelSubsResponse.getMessage() == "Your subscription has been cancelled sucessfully");
	}
}
