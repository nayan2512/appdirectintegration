package com.appdirect.integrationtest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.appdirect.Application;
import com.appdirect.controller.UserController;
import com.appdirect.oauthsecurity.Oauth2LeggedClient;
import com.appdirect.response.AssignUserResponse;
import com.appdirect.response.FailedResponse;
import com.appdirect.response.Response;
import com.appdirect.response.UnAssignUserResponse;
import com.appdirect.service.SubscriptionService;
import com.appdirect.service.UserService;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest("server.port:8090")
public class UserIntegrationTest {

	SubscriptionService subscriptionService;
	
	Oauth2LeggedClient oAuth2LeggedClient; 
	
	UserController subsController;
	
	UserService userService;
	
	@Before
	public void init() throws Exception {
		subsController = new UserController();
		subscriptionService = new SubscriptionService();
		oAuth2LeggedClient = new Oauth2LeggedClient();
		userService = new UserService();
		//create company, user, subscription
	}
	
	@Test
	public void assignUser_ValidEventUrl_Success() throws Exception {
		ResponseEntity<Response> res = subsController.assignUserToSubscription("ABC", "aasah", "QWERTY");
		AssignUserResponse createResponse = (AssignUserResponse) res.getBody();
		assert (res != null);
		assert (createResponse.getMessage() == "User Assigned successfully");
	}
	
	@Test
	public void assignUser_ValidEventUrl_Failure() throws Exception {
		//First create a subscription
		ResponseEntity<Response> res = subsController.assignUserToSubscription("ABC", "aasah", "QWERTY");
		
		//Create second one which would failed
		FailedResponse failedResponse = (FailedResponse) res.getBody();
		assert (res != null);
		assert (failedResponse.getMessage() == "User already exists.");
	}
	
	@Test
	public void unAssignUser_ValidEventUrl_Success() throws Exception {
		ResponseEntity<Response> res = subsController.unAssignUserToSubscription("ABC", "aasah", "QWERTY");
		UnAssignUserResponse createResponse = (UnAssignUserResponse) res.getBody();
		assert (res != null);
		assert (createResponse.getMessage() == "User Assigned successfully");
	}
	
	@Test
	public void unAssignUser_ValidEventUrl_Failure() throws Exception {
		//First create a subscription
		ResponseEntity<Response> res = subsController.unAssignUserToSubscription("ABC", "aasah", "QWERTY");
		
		//Create second one which would failed
		FailedResponse failedResponse = (FailedResponse) res.getBody();
		assert (res != null);
		assert (failedResponse.getMessage() == "User Unassigned successfully");
	}
}
