#AppDirect BackendChallenge

**HOW TO RUN** :-

Go to Application.java and run the application.


**Key Features of this application**:-

**Implemented following APIs** :-

**Subscription APIs** :- 


1) GET /api/v1/subscriptions  (This will create a subscription)


2) DELETE /api/v1/subscriptions  (This will cancel a subscription)


3) PUT /api/v1/subscriptions  (This will change a subscription status)


**Integration test can be found at**
https://bitbucket.org/nayan2512/appdirectintegration/src/4491fadf36e44169161668d8074d30cf0b80eea5/src/test/java/com/appdirect/integrationtest/SubscriptionIntegrationTest.java?at=master&fileviewer=file-view-default


**User APIs** :-


1) POST /api/account/v1/companies/{companyId}/users/{userId}/assign/{subscriptionId}
(This will assign an user to existing subscription)


2) POST /api/account/v1/companies/{companyId}/users/{userId}/unassign/{subscriptionId}
(This will unassign an user from an existing subscription)



**Integration test can be found at**
https://bitbucket.org/nayan2512/appdirectintegration/src/4491fadf36e44169161668d8074d30cf0b80eea5/src/test/java/com/appdirect/integrationtest/UserIntegrationTest.java?at=master&fileviewer=file-view-default



**Swagger API documentation can be found at** :-

http://localhost:8090/api-docs